use std::env;

// Setup:
// complete -o nospace -C /path/to/complete.exe -D

fn main() {
    // Parameters passed in from bash:
    // 1. command, or _EmptycmD_ for none.
    // 2. word being completed
    // 3. word preceding word being completed

    if let Some(word) = env::args().nth(2) {
        let options = glob::MatchOptions {
            case_sensitive: false,
            require_literal_separator: true,
            require_literal_leading_dot: false,
        };

        // Glob for all files starting with the given pattern.
        let pattern = normalise_input(&word) + "*";
        if let Ok(paths) = glob::glob_with(&pattern, options) {
            let dirsonly = is_dironly_command(env::args().nth(1));
            // Ignore errors, filter for directories if required.
            paths
                .filter_map(Result::ok)
                .filter(|p| !dirsonly || p.is_dir())
                .for_each(|p| println!("{}", normalise_output(&p)));
        }
    }
}

fn is_dironly_command(command: Option<String>) -> bool {
    // Let's avoid upper/lower conversions or regex for now.
    command
        .map(|c| c == "cd" || c == "CD" || c == "cD" || c == "Cd")
        .unwrap_or(false)
}

fn normalise_input(input: &str) -> String {
    // Bash "helpfully" converts escaped spaces to "/ ". Convert it back to just space.
    let mut input = str::replace(&input, "/ ", " ");

    // Convert a leading ~ to $HOME/
    if input.starts_with('~') {
        if let Ok(mut home) = std::env::var("HOME") {
            // More mangling - if $HOME starts with a drive specification (eg "/c/") or
            // is just a drive (eg "/c") then replace with standard Windows drive letter
            // specification (eg "c:\") for consistency with other input.
            if home.starts_with('/') && home.len() == 2 || home.get(2..3) == Some("/") {
                match home.get(1..2) {
                    Some(c) if ("a"..="z").contains(&c) || ("A"..="Z").contains(&c) => {
                        let normalised = c.to_owned() + ":\\";
                        home.replace_range(0..2, &normalised)
                    }
                    _ => (),
                }
            }
            input = home + &input[1..];
        }
    }

    input
}

fn normalise_output(path: &std::path::Path) -> String {
    // Make directory names slash separated to work with gitbash.
    let components: Vec<&str> = path.iter().map(|p| p.to_str().unwrap_or("")).collect();
    let mut slashed = components.join("/");

    // Absolute filenames will start with C:/\, which should be replaced with /C
    if path.is_absolute() && &slashed[1..4] == ":/\\" {
        let replacement = format!("/{}", slashed.chars().next().unwrap());
        slashed.replace_range(0..4, &replacement);
    }

    // Append slash for directory - readline can do this, but it is very slow (due to underlying
    // 'stat' call opening the file, and the virus scanner kicking in).
    // Append space otherwise to separate filenames.
    let suffix = if path.is_dir() { '/' } else { ' ' };

    // If filename contains spaces then wrap it in quotes.
    let quote = if slashed.contains(' ') { "\"" } else { "" };
    format!("{}{}{}{}", quote, slashed, suffix, quote)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_dironly_command() {
        for s in ["cd", "CD", "cD", "Cd"] {
            assert!(is_dironly_command(Some(s.to_string())));
        }
        assert!(!is_dironly_command(Some("cdd".to_string())));
        assert!(!is_dironly_command(Some("cdcd".to_string())));
        assert!(!is_dironly_command(Some("abcd".to_string())));
        assert!(!is_dironly_command(None));
    }

    #[test]
    fn test_normalise_input() {
        // No normalisation required.
        assert_eq!(normalise_input("/home/me"), "/home/me");
        assert_eq!(normalise_input("out.t"), "out.t");

        // Filenames with backslash escaped spaces are helpfully normalised before
        // being passed in to replace the backslashes with forward slashes.
        assert_eq!(
            normalise_input("/c/Program/ Files/ (x86)"),
            "/c/Program Files (x86)"
        );

        // Replace leading ~ with $HOME.
        std::env::set_var("HOME", "/test/home");
        assert_eq!(normalise_input("~"), "/test/home");
        assert_eq!(normalise_input("/c/Progra~1"), "/c/Progra~1");
        // If $HOME starts with a drive letter, such as /c/, convert to normal Windows
        // drive format such as C:\
        std::env::set_var("HOME", "/j/one");
        assert_eq!(normalise_input("~/two"), "j:\\/one/two");
        assert_eq!(normalise_input("~"), "j:\\/one");

        std::env::set_var("HOME", "/j/");
        assert_eq!(normalise_input("~/something"), "j:\\//something");
        assert_eq!(normalise_input("~"), "j:\\/");

        std::env::set_var("HOME", "/j");
        assert_eq!(normalise_input("~/something"), "j:\\/something");
        assert_eq!(normalise_input("~"), "j:\\");
    }
}
