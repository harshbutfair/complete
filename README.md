# complete

A reimplementation of command line filename completion for bash.

## Usage

To set the default filename completion:
`complete -o nospace -C /path/to/complete.exe -D`

## Motivation

In Windows, when using the default implementation of bash/readline
with `mark-directories on`, each file is opened to determine the filename.
This is part of the underlying implementation of the `stat` system call in
MSYS2.

Opening a file causes the virus scanner to scan the file. This is slow on
larger files.

## Features

- Only returns directory names for the `cd` function, returns files and
  directories otherwise.
- Case insensitive search, designed for Windows.
- Converts a leading `~` to `$HOME`.
- Handles weird bash mangling of filenames.

## Limitations

- Everything is hardcoded - no configuration possible.
- Includes Windows specific filename handling, but will probably work
  in Linux.

